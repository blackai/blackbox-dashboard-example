/**
 * Created by k on 23.09.17.
 */

var canvas_width = $("#map-projection").parent().width(),
    canvas_height = 420,
    graph_width = $("#map-projection").parent().width() - 20,
    graph_height = 400,
    global_width = 100,
    global_height = 100;


var sampledata = [
    {
        "track_id": "12341234",
        "position": {
            "x": 5,
            "y": 5
        },
        "velocity": {
            "x": 1,
            "y": 1
        },
        "timestamp": new Date()
    }, {
        "track_id": "52345543",
        "position": {
            "x": 5,
            "y": 7
        },
        "velocity": {
            "x": 1,
            "y": 1
        },
        "timestamp": new Date()
    }
];

function generateArc(yaw) {

    var start, end;
    [start, end] = degrees2radian(yaw);

    console.log('yaw:', yaw);
    console.log('start:', start);
    console.log('end:', end);

    var arc = d3.svg.arc()
        .outerRadius(180)
        .innerRadius(15)
        .startAngle(start)
        .endAngle(end);

    return arc;
}


var svgContainer = d3.select("#map-projection").append("svg")
    .attr("id", "canvas")
    .attr("width", canvas_width)
    .attr("height", canvas_height);

var graph_container = svgContainer.append("rect")
    .attr("id", "graph_container")
    .attr("fill", "lightblue")
    .attr("x", 10)
    .attr("y", 10)
    .attr("width", graph_width)
    .attr("height", graph_height)
    .style("border", "1px solid black");

var global_space = svgContainer.append("rect")
    .attr("id", "global-space")
    .attr("fill", "white")
    .attr("x", (graph_width/2 - global_width/2))
    .attr("y", (graph_height/2 - global_height/2))
    .attr("width", global_width)
    .attr("height", global_height)
    .style("border", "1px solid black");

var linked_units = [];
// {% if linked_units %}
// {% for unit in linked_units %}
// linked_units.push({ id: "{{ unit.id }}", xpos : {{ unit.config.xpos }}, ypos: {{ unit.config.ypos }}, yori : {{ unit.config.yori }}, pori : {{ unit.config.pori }}});
// {% endfor %}
// {% endif %}

console.log(linked_units);

var units = svgContainer.selectAll("foo")
    .data(linked_units)
    .enter()
    .append("rect");

var viewpath = svgContainer.selectAll("foo")
    .data(linked_units)
    .enter()
    .append("path");

var unitAttributes = units
    .attr("id", function(d) {return "ui-unit-" + d.id;})
    .attr("x", function(d) {return translateScale(10, 'x', d.xpos); })
    .attr("y", function(d) {return translateScale(10, 'y', d.ypos); })
    .attr("width", 10)
    .attr("height", 10);

var viewAttributes = viewpath
    .attr("class", "unit-fov")
    .attr("id", function(d) {return "ui-unit-fov-" + d.id; })
    .attr("data-yori", function(d) {return d.yori; })
    .attr("data-xpos", function(d) {return d.xpos; })
    .attr("data-ypos", function(d) {return d.ypos; })
    .attr("transform", function(d) {return "translate(" + (5 + translateScale(10, 'x', d.xpos)) + ", " + (5 + translateScale(10, 'y', d.ypos)) + ")"; })
    .attr("fill", "#ffbbbb")

function translateScale(iconPxSize, axis, meters){

    if (axis == 'x') {
        return (10 - iconPxSize / 2) + (meters * 20);
    } else if (axis == 'y') {
        return (global_height - ((10 - iconPxSize / 2) + (meters * 20)));
    }
}

function checkDrawChange() {
    svgElement = document.getElementById("canvas"),
        bb = svgElement.getBBox();

    var htmlContainerWidth = $("#map-projection").parent().width();
    var newHeight = bb.y + bb.height;
    var newWidth = (bb.x + bb.width) > htmlContainerWidth ? (bb.x + bb.width) : htmlContainerWidth;


    svgElement.style.height = newHeight;
    svgElement.style.width = newWidth;

    // Update unit fovs
    $(".unit-fov").each(function(index, obj) {
        $(obj).attr('d', generateArc($(obj).attr('data-yori')));
        $(obj).attr("transform", "translate(" + (5 + translateScale(10, 'x', $(obj).attr('data-xpos'))) + ", " + (5 + translateScale(10, 'y', $(obj).attr('data-ypos'))) + ")");
    });
}

function degrees2radian(centroid){

    centroid = -(parseInt(centroid) - 90) + 270;

    var fov = 70,
        pi = 3.14159,
        upperBound = parseInt(centroid) - (fov/2),
        lowerBound = parseInt(centroid) + (fov/2);

    console.log('centroid', centroid);
    console.log('upperBound', upperBound);
    console.log('lowerBound', lowerBound);

    function converter(input) {
        // Ensure positive degree range
        if (input < 0) {
            input +=360.0;
        }
        return (input / 180 * pi);
    }

    var start = converter(upperBound),
        end = converter(lowerBound);

    return [start, end];
}

// var simulated_tracks = window.setInterval(simulated_track_callback, 60000);

// function simulated_track_callback() {
//
//     // fix number of two tracks
//     for (i = 0; i < 2 ; i++) {
//         // random number between 1 and 9 inclusive
//         var posx = Math.floor(Math.random() * 9) + 1,
//             posy = Math.floor(Math.random() * 9) + 1;
//     }
// }
//
// var persistant_invenue = window.setInterval(simulated_track_callback, 60000);
// var persistant_observed = window.setInterval(simulated_track_callback, 60000);

function simulated_track_callback() {
    // go check API
    console.log("checking weather API");
}

function update_in_venue(tracks){
    console.log('updating person count in venue');
    // complex logic for enter vs exit
    $('#in-venue .count').html((active_tracks.length + Math.ceil(expired_tracks.length/2)));
}

function update_observed(tracks){
    console.log('updating observed people count');
    var count = active_tracks.length;
    $('#observed-count .count').html(count);
}

function update_average_stay(tracks){
    console.log('updating average stay');

}

function update_peak_hours(tracks){
    console.log('updating average stay');
}

function update_half_hour_count(count){
    $('#half-hour-count .count').html(count);
}

function update_daily_count(){
    $('#daily-count .count').html(Math.ceil((active_tracks.length + expired_tracks.length)/2));

}
function update_graph(mapdata) {
    console.log('updating graph');
    // var data = JSON.stringify(mapdata.data);
    // console.log(data);
}

var active_tracks = [];
var expired_tracks = [];
// Run data queries and set up listeners on pageload
$( document ).ready(function() {

    // doConnect("ws://10.1.1.31:1989/");
    doConnect("ws://13.54.13.157:55955/");

    fetchTrackHistory('trackhistory',.5,function (tracks) {
        console.log("track history");
        update_half_hour_count(tracks["count"]);
    });

    //@todo: fix 'none' topic name -> new_tracks
    subscribe('newtrack', '', function(tracks) {

        var track_id = tracks.data.substring(1,37);

        if (active_tracks.indexOf(track_id) < 0) {
            // console.log('new track published.');
            // console.log(track_id);

            active_tracks.push(track_id);
            console.log("active");
            console.log(active_tracks.length);
            // Initiate client persistent subscription to person track data
            subscribe('track', track_id, function(trackdata) {
                console.log('track data returned:');
                var tracks = JSON.stringify(trackdata.data);
                console.log('data: \n' + tracks + '\n');
                update_in_venue(trackdata);
                update_observed(trackdata);
                // update_average_stay(trackdata);
                // update_peak_hours(trackdata);
                update_daily_count();
            });

        } else {
            // @todo: add unsubscribe method for track expiry
        }
    });

    //@todo: fix 'none' topic name -> new_tracks
    subscribe('expiredtrack', '', function(tracks) {

        var track_id = tracks.data.substring(1,37);

        if (active_tracks.indexOf(track_id) > -1) {
            // console.log('removing expired track from active list:');
            // console.log(track_id);
            var track_index = active_tracks.indexOf(track_id);
            active_tracks.splice(track_index, 1);
            expired_tracks.push(track_id);
            console.log("expired");
            console.log(expired_tracks.length);
            // Initiate client persistent subscription to person track data
            unsubscribe('track', track_id, function() {
                console.log('done.');
            });
        } else {
            // @todo: add unsubscribe method for track expiry
        }
    });

    // Initiate client persistent subscription to person track data
    subscribe('floormap', '', function(mapdata) {
        console.log('track data returned.');
        update_graph(mapdata);
    });

    // Historical query for total today
    console.log( "Historical query for total today!" );

    // Historical query for last 30 minutes
    console.log( "Historical query for last 30 minutes!" );

});


checkDrawChange();
