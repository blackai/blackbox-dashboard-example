/**
 * Created by k on 24.09.17.
 */

var callback_functions = {
    'floormap': default_floormap_message,
    'track': default_track_message,
    'newtrack': default_newtrack_message,
    'expiredtrack': default_expiredtrack_message,
    'trackhistory':default_track_history
};

function default_track_history(message) {
    var tracks = JSON.stringify(message.data);
}

function default_expiredtrack_message(message) {
    // What this basically does is loads the latest trackid ready to be subscribed.
}

function default_newtrack_message(message) {
    // What this basically does is loads the latest trackid ready to be subscribed.
}

function default_track_message(message) {
    // Once subscribed, it prints the track information for each client
    var tracks = JSON.stringify(message.data);
    // console.log('tracks: \n' + tracks + '\n');
}

function default_floormap_message(message) {
    //floormap subscription
    console.log("xmin : "+ message.extradata.xmin) ;
    console.log("ymin : "+ message.extradata.ymin) ;
    console.log("xmax : "+ message.extradata.xmax) ;
    console.log("ymax : "+ message.extradata.ymax) ;
    console.log("xdim : "+ message.extradata.xdim) ;
    console.log("ydim : "+ message.extradata.ydim) ;
    console.log("floormap functionality needs to be built \n");
}


function init() {

}

function doConnect(address) {
        websocket = new WebSocket(address);
        websocket.onopen = function (evt) {
            onOpen(evt)
        };
        websocket.onclose = function (evt) {
            onClose(evt)
        };
        websocket.onmessage = function (evt) {
            onMessage(evt)
        };
        websocket.onerror = function (evt) {
            onError(evt)
        };
    }

    function onOpen(evt) {


    }

    function onClose(evt) {

    }

    function onMessage(evt) {
        console.log('message received from blackbox api.');
        if (evt.data instanceof Blob) {
            var reader = new FileReader();
            reader.onload = function () {
                var message = reader.result;
                message = JSON.parse(message);
                // console.log(message);
                callback_functions[message.subscriptionType](message);
            };
            reader.readAsText(evt.data);
        }
    }

    function onError(evt) {
        writeToScreen('error: ' + evt.data + '\n');

        websocket.close();

    }

    window.addEventListener("load", init, false);

    function subscribe(subscriptionType, data, callback) {
        console.log('subscribing to message type: ' + subscriptionType);

        if (typeof callback === "function") {
            console.log("Updating function callback from default for message type: " + subscriptionType);
            callback_functions[subscriptionType] = callback;
        }

        // @todo: remove this timing cue
        websocket.send(JSON.stringify({
            "messageType": "subscribe",
            "subscriptionType": subscriptionType,
            "data": data
        }));

    }

    function fetchTrackHistory(subscriptionType, durationinhours, callback) {
        if (typeof callback === "function") {
            console.log("Updating function callback from default for message type: " + subscriptionType);
            callback_functions[subscriptionType] = callback;
        }
        if (document.readyState === "complete") {
            websocket.send(JSON.stringify({
                "messageType": "subscribe",
                "subscriptionType": subscriptionType,
                "durationinhours": durationinhours
            }));
        }
    }


    function unsubscribe(subscriptionType, data, callback) {
        if (data !== null && data !== "") {

            // if (document.readyState === "complete") {

            websocket.send(JSON.stringify({
                "messageType": "unsubscribe",
                "subscriptionType": subscriptionType,
                "data": data
            }));

            callback();
        }
    }


function doDisconnect() {
    websocket.close();
}